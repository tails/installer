# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Emma Peel, 2019
# Manuela Silva <manuelarodsilva@gmail.com>, 2019-2020
# Manuela Silva <manuelarodsilva@gmail.com>, 2018
# Rui <xymarior@yandex.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-07 08:29+0200\n"
"PO-Revision-Date: 2020-01-05 18:42+0000\n"
"Last-Translator: Manuela Silva <manuelarodsilva@gmail.com>\n"
"Language-Team: Portuguese (Portugal) (http://www.transifex.com/otf/"
"torproject/language/pt_PT/)\n"
"Language: pt_PT\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../tails_installer/creator.py:100
msgid "You must run this application as root"
msgstr "Deve executar esta aplicação como root"

#: ../tails_installer/creator.py:146
msgid "Extracting live image to the target device..."
msgstr "A extrair a imagem live para o dispositivo de destino..."

#: ../tails_installer/creator.py:153
#, python-format
msgid "Wrote to device at %(speed)d MB/sec"
msgstr "Gravado no dispositivo a %(speed)d MB/seg."

#: ../tails_installer/creator.py:296
#, python-format
msgid ""
"There was a problem executing the following command: `%(command)s`.\n"
"A more detailed error log has been written to '%(filename)s'."
msgstr ""
"Ocorreu um erro ao executar o seguinte comando: `%(command)s`.\n"
"Foi gravado um registo mais detalhado do erro em '%(filename)s'."

#: ../tails_installer/creator.py:315
msgid "Verifying SHA1 checksum of LiveCD image..."
msgstr "A verificar a soma de verificação SHA1 da imagem do CD Live..."

#: ../tails_installer/creator.py:319
msgid "Verifying SHA256 checksum of LiveCD image..."
msgstr "A verificar a soma de verificação SHA256 da imagem do CD Live..."

#: ../tails_installer/creator.py:335
msgid ""
"Error: The SHA1 of your Live CD is invalid.  You can run this program with "
"the --noverify argument to bypass this verification check."
msgstr ""
"Erro: a assinatura digital SHA1 do seu CD Live está incorreta. Pode executar "
"este programa com o argumento --noverify para evitar esta verificação."

#: ../tails_installer/creator.py:341
msgid "Unknown ISO, skipping checksum verification"
msgstr "ISO desconhecido, a ignorar a soma de verificação"

#: ../tails_installer/creator.py:353
#, python-format
msgid ""
"Not enough free space on device.\n"
"%dMB ISO + %dMB overlay > %dMB free space"
msgstr ""

#: ../tails_installer/creator.py:360
#, python-format
msgid "Creating %sMB persistent overlay"
msgstr "A criar %sMB da área permanente..."

#: ../tails_installer/creator.py:421
#, python-format
msgid "Unable to copy %(infile)s to %(outfile)s: %(message)s"
msgstr "Não é possível copiar %(infile)s para %(outfile)s: %(message)s"

#: ../tails_installer/creator.py:435
msgid "Removing existing Live OS"
msgstr "A remover o SO Live existente"

#: ../tails_installer/creator.py:444 ../tails_installer/creator.py:457
#, python-format
msgid "Unable to chmod %(file)s: %(message)s"
msgstr "Não é possível executar chmod %(file)s: %(message)s"

#: ../tails_installer/creator.py:450
#, python-format
msgid "Unable to remove file from previous LiveOS: %(message)s"
msgstr "Não é possível remover o ficheiro do LiveOS anterior: %(message)s"

#: ../tails_installer/creator.py:464
#, python-format
msgid "Unable to remove directory from previous LiveOS: %(message)s"
msgstr "Não é possível remover a diretoria do LiveOS anterior: %(message)s"

#: ../tails_installer/creator.py:512
#, python-format
msgid "Cannot find device %s"
msgstr "Não é possível encontrar o dispositivo %s"

#: ../tails_installer/creator.py:713
#, python-format
msgid "Unable to write on %(device)s, skipping."
msgstr "Não é possível gravar no %(device)s, a ignorar."

#: ../tails_installer/creator.py:743
#, python-format
msgid ""
"Some partitions of the target device %(device)s are mounted. They will be "
"unmounted before starting the installation process."
msgstr ""

#: ../tails_installer/creator.py:786 ../tails_installer/creator.py:1010
msgid "Unknown filesystem.  Your device may need to be reformatted."
msgstr ""
"Sistema de ficheiros desconhecido. Poderá ter que reformatar o seu "
"dispositivo."

#: ../tails_installer/creator.py:789 ../tails_installer/creator.py:1013
#, python-format
msgid "Unsupported filesystem: %s"
msgstr "Sistema de ficheiros não suportado: %s"

#: ../tails_installer/creator.py:807
#, python-format
msgid "Unknown GLib exception while trying to mount device: %(message)s"
msgstr ""

#: ../tails_installer/creator.py:812
#, python-format
msgid "Unable to mount device: %(message)s"
msgstr "Não é possível montar o dispositivo: %(message)s"

#: ../tails_installer/creator.py:817
msgid "No mount points found"
msgstr "Não foram encontrados pontos de montagem"

#: ../tails_installer/creator.py:828
#, python-format
msgid "Entering unmount_device for '%(device)s'"
msgstr ""

#: ../tails_installer/creator.py:838
#, python-format
msgid "Unmounting mounted filesystems on '%(device)s'"
msgstr "A desmontar o sistema de ficheiros montados no '%(device)s'"

#: ../tails_installer/creator.py:842
#, python-format
msgid "Unmounting '%(udi)s' on '%(device)s'"
msgstr "A desmontar '%(udi)s' no '%(device)s'"

#: ../tails_installer/creator.py:853
#, python-format
msgid "Mount %s exists after unmounting"
msgstr "O ponto de montagem %s existe depois de desmontado"

#: ../tails_installer/creator.py:866
#, python-format
msgid "Partitioning device %(device)s"
msgstr "A particionar o dispositivo %(device)s"

#: ../tails_installer/creator.py:995
#, python-format
msgid "Unsupported device '%(device)s', please report a bug."
msgstr ""

#: ../tails_installer/creator.py:998
msgid "Trying to continue anyway."
msgstr ""

#: ../tails_installer/creator.py:1007 ../tails_installer/creator.py:1405
msgid "Verifying filesystem..."
msgstr "A verificar o sistema de ficheiros..."

#: ../tails_installer/creator.py:1031
#, python-format
msgid "Unable to change volume label: %(message)s"
msgstr "Não é possível alterar o nome do volume: %(message)s"

#: ../tails_installer/creator.py:1037 ../tails_installer/creator.py:1440
msgid "Installing bootloader..."
msgstr "A instalar o carregador de arranque..."

#: ../tails_installer/creator.py:1064
#, python-format
msgid "Could not find the '%s' COM32 module"
msgstr ""

#: ../tails_installer/creator.py:1072 ../tails_installer/creator.py:1458
#, python-format
msgid "Removing %(file)s"
msgstr "A remover %(file)s"

#: ../tails_installer/creator.py:1186
#, python-format
msgid "%s already bootable"
msgstr "%s já é de arranque"

#: ../tails_installer/creator.py:1206
msgid "Unable to find partition"
msgstr "Não é possível encontrar a partição"

#: ../tails_installer/creator.py:1229
#, python-format
msgid "Formatting %(device)s as FAT32"
msgstr "A formatar o %(device)s como FAT32"

#: ../tails_installer/creator.py:1289
msgid "Could not find syslinux' gptmbr.bin"
msgstr ""

#: ../tails_installer/creator.py:1302
#, python-format
msgid "Reading extracted MBR from %s"
msgstr ""

#: ../tails_installer/creator.py:1306
#, python-format
msgid "Could not read the extracted MBR from %(path)s"
msgstr ""

#: ../tails_installer/creator.py:1319 ../tails_installer/creator.py:1320
#, python-format
msgid "Resetting Master Boot Record of %s"
msgstr "A reiniciar o 'Master Boot Record' de %s"

#: ../tails_installer/creator.py:1325
msgid "Drive is a loopback, skipping MBR reset"
msgstr ""

#: ../tails_installer/creator.py:1329 ../tails_installer/creator.py:1589
#, python-format
msgid "Calculating the SHA1 of %s"
msgstr "A calcular o SHA1 de %s"

#: ../tails_installer/creator.py:1354
msgid "Synchronizing data on disk..."
msgstr "A sincronizar os dados no disco..."

#: ../tails_installer/creator.py:1397
msgid "Error probing device"
msgstr "Erro ao examinar o dispositivo"

#: ../tails_installer/creator.py:1399
msgid "Unable to find any supported device"
msgstr ""

#: ../tails_installer/creator.py:1409
msgid ""
"Make sure your USB key is plugged in and formatted with the FAT filesystem"
msgstr ""

#: ../tails_installer/creator.py:1412
#, python-format
msgid ""
"Unsupported filesystem: %s\n"
"Please backup and format your USB key with the FAT filesystem."
msgstr ""

#: ../tails_installer/creator.py:1481
msgid ""
"Unable to get Win32_LogicalDisk; win32com query did not return any results"
msgstr ""
"Não foi possível obter o Win32_LogicalDisk; a consulta win32com não devolveu "
"quaisquer resultados"

#: ../tails_installer/creator.py:1536
msgid "Cannot find"
msgstr "Não é possível encontrar"

#: ../tails_installer/creator.py:1537
msgid ""
"Make sure to extract the entire tails-installer zip file before running this "
"program."
msgstr ""

#: ../tails_installer/gui.py:69
#, python-format
msgid "Unknown release: %s"
msgstr "Lançamento desconhecido: %s"

#: ../tails_installer/gui.py:73
#, python-format
msgid "Downloading %s..."
msgstr "A transferir %s..."

#: ../tails_installer/gui.py:213
msgid ""
"Error: Cannot set the label or obtain the UUID of your device.  Unable to "
"continue."
msgstr ""
"Erro: não é possível definir o nome ou obter o UUID do seu dispositivo. Não "
"é possível continuar."

#: ../tails_installer/gui.py:260
#, python-format
msgid "Installation complete! (%s)"
msgstr "Instalação completa! (%s)"

#: ../tails_installer/gui.py:265
msgid "Tails installation failed!"
msgstr ""

#: ../tails_installer/gui.py:369
msgid ""
"Warning: This tool needs to be run as an Administrator. To do this, right "
"click on the icon and open the Properties. Under the Compatibility tab, "
"check the \"Run this program as an administrator\" box."
msgstr ""
"Aviso: esta ferramenta precisa de ser executada como um 'Administrador'. "
"Para o fazer, clique direito no ícone e abra as 'Propriedades'. No separador "
"'Compatibilidade', selecione \"Executar como Administrador\"."

#: ../tails_installer/gui.py:381
msgid "Tails Installer"
msgstr ""

#: ../tails_installer/gui.py:441
msgid "Tails Installer is deprecated in Debian"
msgstr ""

#: ../tails_installer/gui.py:443
msgid ""
"To install Tails from scratch, use GNOME Disks instead.\n"
"<a href='https://tails.boum.org/install/linux/usb-overview'>See the "
"installation instructions</a>\n"
"\n"
"To upgrade Tails, do an automatic upgrade from Tails or a manual upgrade "
"from Debian using a second USB stick.\n"
"<a href='https://tails.boum.org/upgrade/tails-overview'>See the manual "
"upgrade instructions</a>"
msgstr ""

#: ../tails_installer/gui.py:450 ../data/tails-installer.ui.h:2
msgid "Clone the current Tails"
msgstr ""

#: ../tails_installer/gui.py:457 ../data/tails-installer.ui.h:3
msgid "Use a downloaded Tails ISO image"
msgstr ""

#: ../tails_installer/gui.py:494 ../tails_installer/gui.py:815
msgid "Upgrade"
msgstr ""

#: ../tails_installer/gui.py:496
msgid "Manual Upgrade Instructions"
msgstr ""

#: ../tails_installer/gui.py:498
msgid "https://tails.boum.org/upgrade/"
msgstr ""

#: ../tails_installer/gui.py:506 ../tails_installer/gui.py:727
#: ../tails_installer/gui.py:792 ../data/tails-installer.ui.h:7
msgid "Install"
msgstr ""

#: ../tails_installer/gui.py:509 ../data/tails-installer.ui.h:1
msgid "Installation Instructions"
msgstr ""

#: ../tails_installer/gui.py:511
msgid "https://tails.boum.org/install/"
msgstr ""

#: ../tails_installer/gui.py:517
#, python-format
msgid "%(size)s %(vendor)s %(model)s device (%(device)s)"
msgstr ""

#: ../tails_installer/gui.py:529
msgid "No ISO image selected"
msgstr ""

#: ../tails_installer/gui.py:530
msgid "Please select a Tails ISO image."
msgstr ""

#: ../tails_installer/gui.py:572
msgid "No device suitable to install Tails could be found"
msgstr ""

#: ../tails_installer/gui.py:574
#, python-format
msgid "Please plug a USB flash drive or SD card of at least %0.1f GB."
msgstr ""

#: ../tails_installer/gui.py:608
#, python-format
msgid ""
"The USB stick \"%(pretty_name)s\" is configured as non-removable by its "
"manufacturer and Tails will fail to start from it. Please try installing on "
"a different model."
msgstr ""

#: ../tails_installer/gui.py:618
#, python-format
msgid ""
"The device \"%(pretty_name)s\" is too small to install Tails (at least "
"%(size)s GB is required)."
msgstr ""

#: ../tails_installer/gui.py:631
#, python-format
msgid ""
"To upgrade device \"%(pretty_name)s\" from this Tails, you need to use a "
"downloaded Tails ISO image:\n"
"https://tails.boum.org/install/download"
msgstr ""

#: ../tails_installer/gui.py:652
msgid "An error happened while installing Tails"
msgstr ""

#: ../tails_installer/gui.py:664
msgid "Refreshing releases..."
msgstr "A atualizar os lançamentos..."

#: ../tails_installer/gui.py:669
msgid "Releases updated!"
msgstr "Lançamentos atualizados!"

#: ../tails_installer/gui.py:722
msgid "Installation complete!"
msgstr "Instalação completa!"

#: ../tails_installer/gui.py:738
msgid "Cancel"
msgstr ""

#: ../tails_installer/gui.py:774
msgid "Unable to mount device"
msgstr "Não é possível montar o dispositivo"

#: ../tails_installer/gui.py:781 ../tails_installer/gui.py:814
msgid "Confirm the target USB stick"
msgstr ""

#: ../tails_installer/gui.py:782
#, python-format
msgid ""
"%(size)s %(vendor)s %(model)s device (%(device)s)\n"
"\n"
"All data on this USB stick will be lost."
msgstr ""

#: ../tails_installer/gui.py:801
#, python-format
msgid "%(parent_size)s %(vendor)s %(model)s device (%(device)s)"
msgstr ""

#: ../tails_installer/gui.py:809
msgid ""
"\n"
"\n"
"The persistent storage on this USB stick will be preserved."
msgstr ""

#: ../tails_installer/gui.py:810
#, python-format
msgid "%(description)s%(persistence_message)s"
msgstr ""

#: ../tails_installer/gui.py:853
msgid "Download complete!"
msgstr "Transferência completa!"

#: ../tails_installer/gui.py:857
msgid "Download failed: "
msgstr "A transferência falhou:"

#: ../tails_installer/gui.py:858
msgid "You can try again to resume your download"
msgstr "Pode tentar retomar a sua transferência"

#: ../tails_installer/gui.py:866
msgid ""
"The selected file is unreadable. Please fix its permissions or select "
"another file."
msgstr ""
"O ficheiro selecionado não é legível. Por favor, corrija as permissões ou "
"selecione outro ficheiro."

#: ../tails_installer/gui.py:872
msgid ""
"Unable to use the selected file.  You may have better luck if you move your "
"ISO to the root of your drive (ie: C:\\)"
msgstr ""
"Não é possível utilizar o ficheiro selecionado. É capaz de ter mais sorte se "
"mover o seu ficheiro ISO para a raiz da sua unidade (ex.: C:\\)."

#: ../tails_installer/gui.py:878
#, python-format
msgid "%(filename)s selected"
msgstr "%(filename)s selecionado"

#: ../tails_installer/source.py:29
msgid "Unable to find LiveOS on ISO"
msgstr ""

#: ../tails_installer/source.py:35
#, python-format
msgid "Could not guess underlying block device: %s"
msgstr ""

#: ../tails_installer/source.py:50
#, python-format
msgid ""
"There was a problem executing `%s`.\n"
"%s\n"
"%s"
msgstr ""

#: ../tails_installer/source.py:64
#, python-format
msgid "'%s' does not exist"
msgstr ""

#: ../tails_installer/source.py:66
#, python-format
msgid "'%s' is not a directory"
msgstr ""

#: ../tails_installer/source.py:76
#, python-format
msgid "Skipping '%(filename)s'"
msgstr ""

#: ../tails_installer/utils.py:55
#, python-format
msgid ""
"There was a problem executing `%s`.%s\n"
"%s"
msgstr ""

#: ../tails_installer/utils.py:135
msgid "Could not open device for writing."
msgstr ""

#: ../data/tails-installer.ui.h:4
msgid "Select a distribution to download:"
msgstr ""

#: ../data/tails-installer.ui.h:5
msgid "Target USB stick:"
msgstr ""

#: ../data/tails-installer.ui.h:6
msgid "Reinstall (delete all data)"
msgstr ""
