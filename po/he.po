# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# aharon, 2012
# Dvir Rosen <ndvnis@gmail.com>, 2014
# Emma Peel, 2019
# ION, 2017-2019
# Johnny Diralenzo, 2015
# Kunda, 2014
# Vladi Pryadko <hunter.pry@gmail.com>, 2013
# yael gogol <yaelgogol@gmail.com>, 2012-2013
# Yaron Shahrabani <sh.yaron@gmail.com>, 2010
# Yuval, 2015
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-07 08:29+0200\n"
"PO-Revision-Date: 2019-11-08 20:24+0000\n"
"Last-Translator: ION\n"
"Language-Team: Hebrew (http://www.transifex.com/otf/torproject/language/"
"he/)\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % "
"1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;\n"

#: ../tails_installer/creator.py:100
msgid "You must run this application as root"
msgstr "אתה חייב להריץ יישום זה כשורש"

#: ../tails_installer/creator.py:146
msgid "Extracting live image to the target device..."
msgstr "מחלץ תמונה חיה אל התקן המטרה…"

#: ../tails_installer/creator.py:153
#, python-format
msgid "Wrote to device at %(speed)d MB/sec"
msgstr "כתב למכשיר במהירות %(speed)d מ״ב/שנייה"

#: ../tails_installer/creator.py:296
#, python-format
msgid ""
"There was a problem executing the following command: `%(command)s`.\n"
"A more detailed error log has been written to '%(filename)s'."
msgstr ""
"הייתה בעיה בביצוע הפקודה הבאה: `%(command)s`.\n"
"יומן שגיאות מפורט יותר נכתב ב-'%(filename)s'."

#: ../tails_installer/creator.py:315
msgid "Verifying SHA1 checksum of LiveCD image..."
msgstr "מוודא סיכום ביקורת SHA1 של תמונת LiveCD…"

#: ../tails_installer/creator.py:319
msgid "Verifying SHA256 checksum of LiveCD image..."
msgstr "מוודא סיכום ביקורת SHA256 של תמונת LiveCD…"

#: ../tails_installer/creator.py:335
msgid ""
"Error: The SHA1 of your Live CD is invalid.  You can run this program with "
"the --noverify argument to bypass this verification check."
msgstr ""
"שגיאה: ה־SHA1 של התקליטור החי שלך בלתי תקף. אתה יכול להריץ תוכנית זו עם "
"הארגומנט ‎--noverify כדי לעקוף בדיקת וידוא זו."

#: ../tails_installer/creator.py:341
msgid "Unknown ISO, skipping checksum verification"
msgstr "ISO בלתי ידוע, מדלג על וידוא סיכום ביקורת"

#: ../tails_installer/creator.py:353
#, python-format
msgid ""
"Not enough free space on device.\n"
"%dMB ISO + %dMB overlay > %dMB free space"
msgstr ""
"אין מספיק שטח פנוי בהתקן.\n"
"%dמ״ב ISO +%dמ״ב כיסוי יתר > %d מ״ב שטח פנוי"

#: ../tails_installer/creator.py:360
#, python-format
msgid "Creating %sMB persistent overlay"
msgstr ""

#: ../tails_installer/creator.py:421
#, python-format
msgid "Unable to copy %(infile)s to %(outfile)s: %(message)s"
msgstr "לא היה ניתן להעתיק את  %(infile)s  אל %(outfile)s :  %(message)s"

#: ../tails_installer/creator.py:435
msgid "Removing existing Live OS"
msgstr "מסיר מערכת הפעלה חיה קיימת"

#: ../tails_installer/creator.py:444 ../tails_installer/creator.py:457
#, python-format
msgid "Unable to chmod %(file)s: %(message)s"
msgstr "לא היה ניתן לשנות הרשאות %(file)s: %(message)s"

#: ../tails_installer/creator.py:450
#, python-format
msgid "Unable to remove file from previous LiveOS: %(message)s"
msgstr "לא היה ניתן להסיר קובץ ממערכת ההפעלה החיה הקודמת: %(message)s"

#: ../tails_installer/creator.py:464
#, python-format
msgid "Unable to remove directory from previous LiveOS: %(message)s"
msgstr "לא היה ניתן להסיר תיקייה ממערכת ההפעלה החיה הקודמת: %(message)s"

#: ../tails_installer/creator.py:512
#, python-format
msgid "Cannot find device %s"
msgstr "לא ניתן למצוא את ההתקן %s"

#: ../tails_installer/creator.py:713
#, python-format
msgid "Unable to write on %(device)s, skipping."
msgstr "לא היה ניתן לכתוב על %(device)s, מדלג."

#: ../tails_installer/creator.py:743
#, python-format
msgid ""
"Some partitions of the target device %(device)s are mounted. They will be "
"unmounted before starting the installation process."
msgstr ""
"מספר מחיצות של התקן המטרה %(device)s מוצבות. הן יהיו בלתי מוצבות לפני התחלת "
"תהליך ההתקנה."

#: ../tails_installer/creator.py:786 ../tails_installer/creator.py:1010
msgid "Unknown filesystem.  Your device may need to be reformatted."
msgstr "מערכת קבצים בלתי ידועה. ההתקן שלך צריך להיות מתוסדר מחדש."

#: ../tails_installer/creator.py:789 ../tails_installer/creator.py:1013
#, python-format
msgid "Unsupported filesystem: %s"
msgstr "מערכת קבצים בלתי נתמכת: %s"

#: ../tails_installer/creator.py:807
#, python-format
msgid "Unknown GLib exception while trying to mount device: %(message)s"
msgstr "חריגת GLib בלתי ידועה בעת ניסיון להציב התקן: %(message)s"

#: ../tails_installer/creator.py:812
#, python-format
msgid "Unable to mount device: %(message)s"
msgstr "לא היה ניתן להציב מכשיר: %(message)s"

#: ../tails_installer/creator.py:817
msgid "No mount points found"
msgstr "לא נמצאו נקודות הצבה"

#: ../tails_installer/creator.py:828
#, python-format
msgid "Entering unmount_device for '%(device)s'"
msgstr "מכניס unmount_device עבור '%(device)s'"

#: ../tails_installer/creator.py:838
#, python-format
msgid "Unmounting mounted filesystems on '%(device)s'"
msgstr "מבטל הצבת מערכות קבצים המוצבות על '%(device)s'"

#: ../tails_installer/creator.py:842
#, python-format
msgid "Unmounting '%(udi)s' on '%(device)s'"
msgstr "מבטל הצבת '%(udi)s' על '%(device)s'"

#: ../tails_installer/creator.py:853
#, python-format
msgid "Mount %s exists after unmounting"
msgstr "הצבת %s קיימת לאחר ביטול הצבה"

#: ../tails_installer/creator.py:866
#, python-format
msgid "Partitioning device %(device)s"
msgstr "יוצר מחיצות על התקן %(device)s"

#: ../tails_installer/creator.py:995
#, python-format
msgid "Unsupported device '%(device)s', please report a bug."
msgstr "התקן בלתי נתמך '%(device)s', אנא דווח על תקל."

#: ../tails_installer/creator.py:998
msgid "Trying to continue anyway."
msgstr "מנסה להמשיך בכל זאת."

#: ../tails_installer/creator.py:1007 ../tails_installer/creator.py:1405
msgid "Verifying filesystem..."
msgstr "מוודא מערכת קבצים…"

#: ../tails_installer/creator.py:1031
#, python-format
msgid "Unable to change volume label: %(message)s"
msgstr "לא היה ניתן לשנות תוית עוצמת קול: %(message)s"

#: ../tails_installer/creator.py:1037 ../tails_installer/creator.py:1440
msgid "Installing bootloader..."
msgstr ""

#: ../tails_installer/creator.py:1064
#, python-format
msgid "Could not find the '%s' COM32 module"
msgstr "לא היה ניתן למצוא את פירקן ה-COM32 '%s'"

#: ../tails_installer/creator.py:1072 ../tails_installer/creator.py:1458
#, python-format
msgid "Removing %(file)s"
msgstr "מסיר את %(file)s"

#: ../tails_installer/creator.py:1186
#, python-format
msgid "%s already bootable"
msgstr "%s כבר בר־אתחול"

#: ../tails_installer/creator.py:1206
msgid "Unable to find partition"
msgstr "לא היה ניתן למצוא מחיצה"

#: ../tails_installer/creator.py:1229
#, python-format
msgid "Formatting %(device)s as FAT32"
msgstr "תסדור %(device)s כ-FAT32"

#: ../tails_installer/creator.py:1289
msgid "Could not find syslinux' gptmbr.bin"
msgstr "לא ניתן היה למצוא את syslinux' gptmbr.bin"

#: ../tails_installer/creator.py:1302
#, python-format
msgid "Reading extracted MBR from %s"
msgstr "קורא MBR מחולץ מ-%s"

#: ../tails_installer/creator.py:1306
#, python-format
msgid "Could not read the extracted MBR from %(path)s"
msgstr "לא היה ניתן לקרוא את ה-MBR המחולץ מן %(path)s"

#: ../tails_installer/creator.py:1319 ../tails_installer/creator.py:1320
#, python-format
msgid "Resetting Master Boot Record of %s"
msgstr "מאפס רשומת אתחול‏־אב של %s"

#: ../tails_installer/creator.py:1325
msgid "Drive is a loopback, skipping MBR reset"
msgstr "הכונן הוא לולאה חוזרת, מדלג על איפוס MBR"

#: ../tails_installer/creator.py:1329 ../tails_installer/creator.py:1589
#, python-format
msgid "Calculating the SHA1 of %s"
msgstr "מחשב את ה-SHA1 של %s"

#: ../tails_installer/creator.py:1354
msgid "Synchronizing data on disk..."
msgstr "מסנכרן נתונים על הדיסק.."

#: ../tails_installer/creator.py:1397
msgid "Error probing device"
msgstr "התקן הגורם לשגיאה"

#: ../tails_installer/creator.py:1399
msgid "Unable to find any supported device"
msgstr "לא היה ניתן למצוא התקן נתמך כלשהו"

#: ../tails_installer/creator.py:1409
msgid ""
"Make sure your USB key is plugged in and formatted with the FAT filesystem"
msgstr "וודא שמפתח ה־USB שלך מחובר ומתוסדר עם מערכת הקבצים FAT"

#: ../tails_installer/creator.py:1412
#, python-format
msgid ""
"Unsupported filesystem: %s\n"
"Please backup and format your USB key with the FAT filesystem."
msgstr ""
"מערכת קבצים בלתי נתמכת: %s\n"
"אנא גבה ותסדר את מפתח ה־USB שלך עם מערכת הקבצים FAT."

#: ../tails_installer/creator.py:1481
msgid ""
"Unable to get Win32_LogicalDisk; win32com query did not return any results"
msgstr ""
"לא היה ניתן להשיג את Win32_LogicalDisk; שאילתת win32com לא החזירה תוצאות "
"כלשהן"

#: ../tails_installer/creator.py:1536
msgid "Cannot find"
msgstr "לא יכול למצוא"

#: ../tails_installer/creator.py:1537
msgid ""
"Make sure to extract the entire tails-installer zip file before running this "
"program."
msgstr "וודא לחלץ את כל קובץ ה-zip בשם tails-installer לפני הרצת תוכנית זו."

#: ../tails_installer/gui.py:69
#, python-format
msgid "Unknown release: %s"
msgstr "שחרור בלתי ידוע: %s"

#: ../tails_installer/gui.py:73
#, python-format
msgid "Downloading %s..."
msgstr "מוריד את %s…"

#: ../tails_installer/gui.py:213
msgid ""
"Error: Cannot set the label or obtain the UUID of your device.  Unable to "
"continue."
msgstr ""
"שגיאה לא ניתן להגדיר את התווית או לקבל את ה־UUID של ההתקן שלך. לא היה ניתן "
"להמשיך."

#: ../tails_installer/gui.py:260
#, python-format
msgid "Installation complete! (%s)"
msgstr "ההתקנה הושלמה! (%s)"

#: ../tails_installer/gui.py:265
msgid "Tails installation failed!"
msgstr "התקנת Tails נכשלה!"

#: ../tails_installer/gui.py:369
msgid ""
"Warning: This tool needs to be run as an Administrator. To do this, right "
"click on the icon and open the Properties. Under the Compatibility tab, "
"check the \"Run this program as an administrator\" box."
msgstr ""
"אזהרה: כלי זה צריך להיות מופעל כמנהלן. כדי לעשות זאת, לחץ לחיצה ימנית על "
"הצלמית ופתח את המאפיינים. תחת הלשונית 'תאימות', סמן את התיבה \"הפעל תוכנית "
"זו כמנהל מערכת\"."

#: ../tails_installer/gui.py:381
msgid "Tails Installer"
msgstr "מתקין Tails"

#: ../tails_installer/gui.py:441
msgid "Tails Installer is deprecated in Debian"
msgstr ""

#: ../tails_installer/gui.py:443
msgid ""
"To install Tails from scratch, use GNOME Disks instead.\n"
"<a href='https://tails.boum.org/install/linux/usb-overview'>See the "
"installation instructions</a>\n"
"\n"
"To upgrade Tails, do an automatic upgrade from Tails or a manual upgrade "
"from Debian using a second USB stick.\n"
"<a href='https://tails.boum.org/upgrade/tails-overview'>See the manual "
"upgrade instructions</a>"
msgstr ""

#: ../tails_installer/gui.py:450 ../data/tails-installer.ui.h:2
msgid "Clone the current Tails"
msgstr "שבט את ה-Tails הנוכחי"

#: ../tails_installer/gui.py:457 ../data/tails-installer.ui.h:3
msgid "Use a downloaded Tails ISO image"
msgstr "השתמש בתמונת ISO שהורדה של Tails"

#: ../tails_installer/gui.py:494 ../tails_installer/gui.py:815
msgid "Upgrade"
msgstr "שדרג"

#: ../tails_installer/gui.py:496
msgid "Manual Upgrade Instructions"
msgstr "הוראות שדרוג ידני"

#: ../tails_installer/gui.py:498
msgid "https://tails.boum.org/upgrade/"
msgstr "https://tails.boum.org/upgrade/"

#: ../tails_installer/gui.py:506 ../tails_installer/gui.py:727
#: ../tails_installer/gui.py:792 ../data/tails-installer.ui.h:7
msgid "Install"
msgstr "התקן"

#: ../tails_installer/gui.py:509 ../data/tails-installer.ui.h:1
msgid "Installation Instructions"
msgstr "הוראות התקנה"

#: ../tails_installer/gui.py:511
msgid "https://tails.boum.org/install/"
msgstr "https://tails.boum.org/install/"

#: ../tails_installer/gui.py:517
#, python-format
msgid "%(size)s %(vendor)s %(model)s device (%(device)s)"
msgstr "%(size)s %(vendor)s %(model)s התקן (%(device)s)"

#: ../tails_installer/gui.py:529
msgid "No ISO image selected"
msgstr "לא נבחרה תמונת ISO"

#: ../tails_installer/gui.py:530
msgid "Please select a Tails ISO image."
msgstr "אנא בחר תמונת ISO של Tails."

#: ../tails_installer/gui.py:572
msgid "No device suitable to install Tails could be found"
msgstr "לא היה ניתן למצוא התקן הולם להתקין את Tails"

#: ../tails_installer/gui.py:574
#, python-format
msgid "Please plug a USB flash drive or SD card of at least %0.1f GB."
msgstr "אנא חבר כונן החסן USB או כרטיס SD של לפחות %0.1f ג״ב."

#: ../tails_installer/gui.py:608
#, python-format
msgid ""
"The USB stick \"%(pretty_name)s\" is configured as non-removable by its "
"manufacturer and Tails will fail to start from it. Please try installing on "
"a different model."
msgstr ""

#: ../tails_installer/gui.py:618
#, python-format
msgid ""
"The device \"%(pretty_name)s\" is too small to install Tails (at least "
"%(size)s GB is required)."
msgstr ""
"ההתקן \"%(pretty_name)s\" קטן מדי כדי להתקין עליו את Tails (לפחות %(size)s "
"ג״ב דרושים)."

#: ../tails_installer/gui.py:631
#, python-format
msgid ""
"To upgrade device \"%(pretty_name)s\" from this Tails, you need to use a "
"downloaded Tails ISO image:\n"
"https://tails.boum.org/install/download"
msgstr ""
"כדי לשדרג את התקן \"%(pretty_name)s\" מתוך Tails זה, אתה צריך להשתמש בתמונת "
"ISO ברת־הורדה של Tails:\n"
"https://tails.boum.org/install/download"

#: ../tails_installer/gui.py:652
msgid "An error happened while installing Tails"
msgstr "שגיאה התרחשה בעת התקנת Tails"

#: ../tails_installer/gui.py:664
msgid "Refreshing releases..."
msgstr "מרענן שחרורים…"

#: ../tails_installer/gui.py:669
msgid "Releases updated!"
msgstr "שחרורים עודכנו!"

#: ../tails_installer/gui.py:722
msgid "Installation complete!"
msgstr "התקנה הושלמה!"

#: ../tails_installer/gui.py:738
msgid "Cancel"
msgstr "בטל"

#: ../tails_installer/gui.py:774
msgid "Unable to mount device"
msgstr "לא היה ניתן להציב התקן."

#: ../tails_installer/gui.py:781 ../tails_installer/gui.py:814
msgid "Confirm the target USB stick"
msgstr "אשר את החסן ה-USB של המטרה"

#: ../tails_installer/gui.py:782
#, python-format
msgid ""
"%(size)s %(vendor)s %(model)s device (%(device)s)\n"
"\n"
"All data on this USB stick will be lost."
msgstr ""
"%(size)s %(vendor)s %(model)s התקן (%(device)s)\n"
"\n"
"כל הנתונים בהחסן USB זה יאבדו."

#: ../tails_installer/gui.py:801
#, python-format
msgid "%(parent_size)s %(vendor)s %(model)s device (%(device)s)"
msgstr "%(parent_size)s %(vendor)s %(model)s התקן (%(device)s)"

#: ../tails_installer/gui.py:809
msgid ""
"\n"
"\n"
"The persistent storage on this USB stick will be preserved."
msgstr ""
"\n"
"\n"
"האחסון המתמיד בהחסן USB זה ישומר."

#: ../tails_installer/gui.py:810
#, python-format
msgid "%(description)s%(persistence_message)s"
msgstr "%(description)s%(persistence_message)s"

#: ../tails_installer/gui.py:853
msgid "Download complete!"
msgstr "הורדה הושלמה!"

#: ../tails_installer/gui.py:857
msgid "Download failed: "
msgstr "הורדה נכשלה:"

#: ../tails_installer/gui.py:858
msgid "You can try again to resume your download"
msgstr "אתה יכול לנסות שוב להמשיך את ההורדה שלך"

#: ../tails_installer/gui.py:866
msgid ""
"The selected file is unreadable. Please fix its permissions or select "
"another file."
msgstr "הקובץ שנבחר אינו קריא. אנא תקן את הרשאותיו או בחר קובץ אחר."

#: ../tails_installer/gui.py:872
msgid ""
"Unable to use the selected file.  You may have better luck if you move your "
"ISO to the root of your drive (ie: C:\\)"
msgstr ""
"לא היה ניתן להשתמש בקובץ הנבחר. יהיה לך אולי יותר מזל אם תעביר את ה-ISO שלך "
"לשורש של הכונן שלך (למשל: \\:C)"

#: ../tails_installer/gui.py:878
#, python-format
msgid "%(filename)s selected"
msgstr "%(filename)s נבחר"

#: ../tails_installer/source.py:29
msgid "Unable to find LiveOS on ISO"
msgstr "לא היה ניתן למצוא LiveOS על ISO"

#: ../tails_installer/source.py:35
#, python-format
msgid "Could not guess underlying block device: %s"
msgstr "לא היה יכול לנחש התקן של גוש משתמע: %s"

#: ../tails_installer/source.py:50
#, python-format
msgid ""
"There was a problem executing `%s`.\n"
"%s\n"
"%s"
msgstr ""
"הייתה שגיאה בביצוע `%s`.\n"
"%s\n"
"%s"

#: ../tails_installer/source.py:64
#, python-format
msgid "'%s' does not exist"
msgstr "'%s' אינו קיים"

#: ../tails_installer/source.py:66
#, python-format
msgid "'%s' is not a directory"
msgstr "'%s' אינו סיפרייה"

#: ../tails_installer/source.py:76
#, python-format
msgid "Skipping '%(filename)s'"
msgstr "מדלג על '%(filename)s'"

#: ../tails_installer/utils.py:55
#, python-format
msgid ""
"There was a problem executing `%s`.%s\n"
"%s"
msgstr ""
"הייתה שגיאה בביצוע `%s`.%s\n"
"%s"

#: ../tails_installer/utils.py:135
msgid "Could not open device for writing."
msgstr "לא היה יכול לפתוח התקן עבור כתיבה."

#: ../data/tails-installer.ui.h:4
msgid "Select a distribution to download:"
msgstr "בחר הפצה להורדה:"

#: ../data/tails-installer.ui.h:5
msgid "Target USB stick:"
msgstr "החסן USB מטרה:"

#: ../data/tails-installer.ui.h:6
msgid "Reinstall (delete all data)"
msgstr "התקן מחדש (מחק כל הנתונים)"
